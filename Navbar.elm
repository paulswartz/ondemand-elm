module Navbar (navbar) where
import Html exposing (..)
import Html.Attributes exposing (..)

navbar : Html
navbar =
  node "nav" [ class "navbar navbar-default" ]
       [ div [ class "container-fluid" ]
             [ div [ class "navbar-header" ]
                     [ a [ class "navbar-brand"
                         , href "javascript:window.location.reload()" ]
                         [ text "OnDemand" ]
                     , p [ class "navbar-text navbar-right" ]
                           [ a [ class "navbar-link"
                               , href "https://gitlab.com/paulswartz/ondemand-elm"
                               , target "_blank" ]
                             [ text "About" ] ]
                     ]
             ]
       ]
