module Action where

import Model exposing (MasonResult)
import Mason.Render
import Time

type Action = Loaded MasonResult
            | Control Mason.Render.Action
            | Back
            | CurrentTime Time.Time
