module Schema where

import Result
import String
import Json.Decode as Json exposing ((:=))

type Value = JsonString String | JsonNumber Float | Unknown
type alias Property =
  { name: String
  , title: Maybe String
  , description: Maybe String
  , value: Value }
type alias Schema = List Property

defaultValue jsonFormat =
  case jsonFormat of
    "number" -> JsonNumber 0
    "string" -> JsonString ""
    _ -> Unknown

empty = []

asString : Value -> String
asString value =
  case value of
    JsonString s -> s
    JsonNumber n -> toString n
    Unknown -> ""

fromString : Value -> String -> Maybe Value
fromString original string =
  case original of
    JsonString _ -> Just (JsonString string)
    JsonNumber _ -> String.toFloat string
                           |> Result.toMaybe
                           |> Maybe.map JsonNumber
    Unknown -> Just Unknown

update : Property -> Value -> Property
update p v = { p | value = v }

updateFromString : Property -> String -> Property
updateFromString p s =
  case fromString p.value s of
    Just v -> update p v
    Nothing -> p


decoder : Json.Decoder Schema
decoder =
  let
    mapper (name, property) = {property | name = name}
  in
    Json.map
        (List.map mapper)
        ("properties" := (Json.keyValuePairs propertyDecoder))



propertyDecoder : Json.Decoder Property
propertyDecoder =
  Json.object4 Property
      (Json.succeed "")
      (Json.maybe ("title" := Json.string))
      (Json.maybe ("description" := Json.string))
      (("type" := Json.string) `Json.andThen` defaultValueDecoder)


defaultValueDecoder : String -> Json.Decoder Value
defaultValueDecoder format = Json.succeed <| defaultValue format
