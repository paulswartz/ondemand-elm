module Mason where

import Json.Decode as Json exposing ((:=))
import Dict exposing (..)
import String exposing (..)
import Schema exposing (Schema)

type Encoding = JSON | None

type alias Mason =
  {
    namespaces: Namespaces
  ,  controls: Controls
  , value: Json.Value
  }

type alias Control =
  { relation: String
  , title: Maybe String
  , description: Maybe String
  , method: String
  , href: String
  , schema : Schema
  , encoding : Encoding
  , template : Maybe Json.Value
  }


type alias Namespaces = Dict String String
type alias Controls =  List Control

decoder : Json.Decoder Mason
decoder =
    Json.object3 maybeCombineNamespaces
          (Json.maybe ("@namespaces" := Json.dict namespaceDecoder))
          (Json.maybe ("@controls" := Json.dict controlDecoder))
          Json.value

namespaceDecoder : Json.Decoder String
namespaceDecoder = "name" := Json.string

controlDecoder : Json.Decoder Control
controlDecoder = Json.object8 Control
                 (Json.succeed "") -- temporary relation
                 (Json.maybe ("title" := Json.string))
                 (Json.maybe ("description" := Json.string))
                 (Json.oneOf ["method" := Json.string, Json.succeed "GET"])
                 ("href" := Json.string)
                 (Json.oneOf
                        [ (("isHrefTemplate" := Json.bool) `Json.andThen` maybeDecodeSchema)
                        , Json.succeed Schema.empty ])
                 (Json.oneOf
                        [ Json.map decodeEncoding ("encoding" := Json.string)
                        , Json.succeed None ])
                 (Json.maybe ("template" := Json.value))

maybeCombineNamespaces maybeNamespaces maybeControls =
  let
    namespaces = Maybe.withDefault Dict.empty maybeNamespaces
    controls = Maybe.withDefault Dict.empty maybeControls
  in
    combineNamespaces namespaces controls

combineNamespaces namespaces controls =
  Mason namespaces (fixControls namespaces controls)


fixControls : Namespaces -> Dict String Control -> Controls
fixControls namespaces controls =
  addRelations <| remapKeys (addNamespaceToKey namespaces) controls

addRelations : Dict String Control -> Controls
addRelations =
  let
    folder key value =
      (::) { value | relation = key }
  in
    Dict.foldl folder []

remapKeys mapFunction =
  let
    folder key value =
      Dict.insert (mapFunction key) value
  in
    Dict.foldl folder Dict.empty

addNamespaceToKey : Namespaces -> String -> String
addNamespaceToKey namespaces key =
  let
    splitKey = split ":" key
  in
    case splitKey of
      [] -> key
      [x] -> key
      x::xs ->
        case get x namespaces of
          Just namespace -> namespace ++ (join ":" xs)
          Nothing -> key

maybeDecodeSchema : Bool -> Json.Decoder Schema
maybeDecodeSchema isHrefTemplate =
  if isHrefTemplate
  then
    ("schema" := Schema.decoder)
  else
    Json.succeed Schema.empty

decodeEncoding : String -> Encoding
decodeEncoding encoding =
  if encoding == "json"
  then
     JSON
  else
    None


-- helpers
combineParentNamespaces : Mason -> Mason -> Mason
combineParentNamespaces parent child =
  { child | controls = List.map (addNamespacesToControl parent.namespaces) child.controls }

addNamespacesToControl : Namespaces -> Control -> Control
addNamespacesToControl namespaces control =
  let
    add = addNamespaceToKey namespaces
  in
    { control | relation = add control.relation }
