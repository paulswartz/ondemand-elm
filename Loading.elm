module Loading where
import Mason
import Dict
import Json.Encode

result url =
  Ok <| Mason.Mason Dict.empty [] (Json.Encode.string <| url)
