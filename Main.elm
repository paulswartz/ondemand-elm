import App exposing (app)
import Task
import Effects exposing (Never)
main =
  app.html


port tasks : Signal (Task.Task Never ())
port tasks =
  app.tasks
