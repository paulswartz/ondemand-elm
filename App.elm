module App where

import StartApp
import Effects exposing (..)
import Task
import Model exposing (..)
import Action exposing (..)

import Html exposing (div, node, Html)
import Html.Attributes exposing (rel, href)
import Http
import List
import Dict
import Time

import Mason.Render
import Mason.Control

import Navbar
import Loading
import Views.Model


css : String -> Html
css path =
  node "link" [ rel "stylesheet", href path ] []

outerView : Signal.Address Action -> Model -> Html
outerView address model =
  div []
        [ css "/styles/bootstrap.min.css"
        , css "/styles/back.css"
        , Navbar.navbar
        , (Views.Model.view address model) ]


init : (Model, Effects Action)
init =
  ( initialModel
  , Effects.none )



update : Action -> Model -> (Model, Effects Action)
update action model =
  let
    newResult model =
      case model.history of
        [] -> model.result
        x::xs -> Ok x

    removeHistory model =
      case model.history of
        [] -> []
        x::xs -> xs
  in
    case action of
      Control action -> updateControl action model
      Loaded result -> ( { model | result = result }
                       , Effects.none)
      Back -> ( { model | result = newResult model
                , history = removeHistory model }
              , Effects.none )
      CurrentTime time -> ( { model | currentTime = time }
                          , Effects.none )


updateControl : Mason.Render.Action -> Model -> (Model, Effects Action)
updateControl action model =
  let
    addHistory model =
      case model.result of
        Err _ -> model.history
        Ok x -> x :: model.history
  in
  case action of
    Mason.Render.Activated control ->
      ( { model |
          url = Mason.Control.urlFor control
        , result = Loading.result (Mason.Control.urlFor control)
        , history = addHistory model }
      , httpToEffect (Mason.Control.toTask control) )
    Mason.Render.Updated control ->
      let
        mapper c =
          if c.relation == control.relation then
             control
          else
            c
      in
        ( { model | result = Result.map (\mason -> {
            mason | controls = List.map mapper mason.controls
                                         }) model.result
          }
        , Effects.none )

httpToEffect http =
  http
    |> Task.toResult
    |> Task.map (Result.formatError (\x -> "http error: " ++ (toString x)))
    |> Task.map Loaded
    |> Effects.task

app =
  StartApp.start
    { init = init
    , update = update
    , view = outerView
    , inputs = [Signal.map CurrentTime (Time.every (Time.second))]
    }
