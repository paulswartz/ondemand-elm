module Model  (MasonResult, Model, initialModel) where

import Mason exposing (Mason)
import Initial
import Time exposing (Time)

type alias MasonResult = Result String Mason

type alias Model =
  { url: String
  , result: MasonResult
  , history: List Mason
  , currentTime: Time
  }

initialModel : Model
initialModel =
  Model "" (Ok <| Initial.mason) [] 0
