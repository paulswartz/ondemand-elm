module Views.Reservation (decodeToView) where

import Html exposing (div, h2, text, Html)
import Html.Attributes exposing (class)
import Json.Decode as Json exposing ((:=))
import Mason.Render
import Date
import Time exposing (Time, inMinutes, inSeconds)
import String exposing (padLeft)
import Model exposing (Model)

type alias Reservation =
  { id: Int
  , expirationTime: Maybe (Result String Time)
  }


decoder : Json.Decoder Reservation
decoder = Json.object2 Reservation
          ("id" := Json.int)
          (Json.maybe ("expirationTime" := decodeTime))

decodeTime : Json.Decoder (Result String Time)
decodeTime =
  let
    stringToTime s =
      Result.map Date.toTime (Date.fromString s)
  in
    Json.map stringToTime Json.string

view : Signal.Address Mason.Render.Action -> Reservation -> Model -> Html
view address reservation model =
  div [ class "row" ]
        [ div [ class "col-xs-12 col-md-6" ]
              [ Maybe.withDefault (h2 [] [text "Enjoy your drive!"]) (Maybe.map (\x -> expiration x model.currentTime) reservation.expirationTime) ]
        ]

expiration : Result String Time -> Time -> Html
expiration result current =
  case result of
    Err err -> text ("Unable to parse: " ++ err)
    Ok time -> h2 [] [ text ("Expires in: " ++ (timeInMinutesSeconds (time - current))) ]

timeInMinutesSeconds time =
  let
    minutes = floor (inMinutes time)

    seconds = (floor (inSeconds time)) - (minutes * 60)
  in
    (toString minutes) ++ ":" ++ (padLeft 2 '0' (toString seconds))

decodeToView : Json.Decoder (Signal.Address Mason.Render.Action -> Model -> Html)
decodeToView =
  decoder `Json.andThen` \x -> Json.succeed ((flip view) x)
