module Views.Loading (decodeToView) where

import Html exposing (div, h2, h3, text, span, Html)
import Html.Attributes exposing (class, style)
import Json.Decode as Json
import Mason.Render
import Model exposing (Model)
import Views.ProgressBar exposing (progressBar)

type alias Loading = String

decoder : Json.Decoder Loading
decoder = Json.string

view : Signal.Address Mason.Render.Action -> Loading -> Model -> Html
view address loading model =
  div [] [ loadingBar loading ]

loadingBar : Loading -> Html
loadingBar loading =
  progressBar "progress-bar-striped" "90%" ("Loading " ++ loading ++ "...")

decodeToView : Json.Decoder (Signal.Address Mason.Render.Action -> Model -> Html)
decodeToView =
  decoder `Json.andThen` \x -> Json.succeed ((flip view) x)
