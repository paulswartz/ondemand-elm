module Views.Root (decodeToView) where

import Html exposing (div, pre, text, iframe, Html)
import Json.Decode as Json exposing ((:=))
import Mason
import Mason.Render exposing (renderControl)
import Model exposing (Model)

type alias Root =
  { mason: Mason.Mason
  , upcomingReservations: List Mason.Mason
  }

decoder : Json.Decoder Root
decoder = Json.object2 Root
          Mason.decoder
          ("upcomingReservations" := Json.list Mason.decoder)

view : Signal.Address Mason.Render.Action -> Root -> Model -> Html
view address root model =
  div [] [ upcomingView address root.upcomingReservations ]

upcomingView : Signal.Address Mason.Render.Action -> List Mason.Mason -> Html
upcomingView address upcomingReservations =
  div [] (List.map (reservationView address) upcomingReservations)

reservationView : Signal.Address Mason.Render.Action -> Mason.Mason -> Html
reservationView address upcomingReservation =
  div [] (List.map (renderControl address) upcomingReservation.controls)

decodeToView : Json.Decoder (Signal.Address Mason.Render.Action -> Model -> Html)
decodeToView =
  decoder `Json.andThen` \x -> Json.succeed ((flip view) x)
