module Views.Vehicle (decodeToView) where

import Html exposing (div, h2, text, img, Html)
import Html.Attributes exposing (src, alt)
import Json.Decode as Json
import Mason.Render
import Views.VehicleResults as VehicleResults
import Model exposing (Model)

view : Signal.Address Mason.Render.Action -> VehicleResults.Vehicle -> Model -> Html
view address vehicle model =
  let
    name = vehicle.model ++ " " ++ vehicle.licensePlate
  in
    div [] [ h2 [] [ text name ]
           , img [ src vehicle.image
                 , alt ("Image of " ++ name) ] [] ]

decodeToView : Json.Decoder (Signal.Address Mason.Render.Action -> Model -> Html)
decodeToView =
  VehicleResults.vehicleDecoder `Json.andThen` \x -> Json.succeed ((flip view) x)
