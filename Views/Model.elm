module Views.Model where

import Html exposing (div, pre, text, button, Html)
import Html.Attributes exposing (class)
import Html.Events exposing (onClick)

import Json.Decode as Json
import Json.Encode

import Mason
import Mason.Control
import Mason.Render exposing (renderControl)
import Model exposing (Model)
import Action exposing (..)

import Views.VehicleStatus
import Views.VehicleResults
import Views.Vehicle
import Views.Root
import Views.Loading
import Views.Reservation
import Views.ParkingSpots
import Views.ParkingSpot


view : Signal.Address Action -> Model -> Html
view address model =
  let
    render =
      renderControl (Signal.forwardTo address Control)
  in
    case model.result of
      Err err -> errorView err
      Ok mason -> wrapWithContainer
                  [ valueView address model mason.value [ Views.VehicleResults.decodeToView
                                                        , Views.VehicleStatus.decodeToView
                                                        , Views.Vehicle.decodeToView
                                                        , Views.ParkingSpot.decodeToView
                                                        , Views.ParkingSpots.decodeToView
                                                        , Views.Root.decodeToView
                                                        , Views.Reservation.decodeToView
                                                        , Views.Loading.decodeToView
                                                        ]
                  , div [] (List.map render mason.controls)
                  , div [] [(historyView address model.history)] ]


valueView : Signal.Address Action -> Model -> Json.Value -> List (Json.Decoder (Signal.Address Mason.Render.Action -> Model -> Html)) -> Html
valueView address model value decoders =
  case Json.decodeValue (Json.oneOf decoders) value of
    Err err -> defaultValueView value
    Ok view -> view (Signal.forwardTo address Control) model

defaultValueView : Json.Value -> Html
defaultValueView value =
  pre [] [text <| Json.Encode.encode 4 value]


historyView : Signal.Address Action -> List Mason.Mason -> Html
historyView address history =
  case history of
    [] -> div [] []
    x::xs -> button [class "btn btn-sm btn-back" , onClick address Back ] [text "Back"]


errorView : String -> Html
errorView err =
  pre [] [text err]


wrapWithContainer contents =
  div [ class "container-fluid" ]
      [ div [ class "row" ]
            [ div [ class "col-xs-12" ] contents
            ]
      ]
