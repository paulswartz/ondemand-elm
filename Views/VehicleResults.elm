module Views.VehicleResults (decodeToView, Vehicle, vehicleDecoder, view) where

import Json.Decode as Json exposing ((:=))
import Html exposing (ul, li, text, h2, table, tr, td, tbody, img, Html)
import Html.Attributes exposing (class, src, alt)
import Mason
import Mason.Control
import Mason.Render exposing (renderControl)
import Model exposing (Model)

type alias Vehicle =
  { id: Int
  , model: String
  , image: String
  , licensePlate: String
  , mason: Mason.Mason}

type alias Results = List Vehicle

decoder : Json.Decoder Results
decoder =
  ("vehicles" := Json.list vehicleDecoder)

vehicleDecoder : Json.Decoder Vehicle
vehicleDecoder = Json.object5 Vehicle
                 ("id" := Json.int)
                 ("model" := Json.string)
                 ("image" := Json.string)
                 (Json.oneOf [("licensePlate" := Json.string), (Json.succeed "")])
                 Mason.decoder

view : Signal.Address Mason.Render.Action -> Results -> Model -> Html
view address results model =
  table [ class "table table-bordered" ]
          [ tbody [] <| List.map (vehicleView address model) results ]

vehicleView : Signal.Address Mason.Render.Action -> Model -> Vehicle -> Html
vehicleView address model vehicle =
  let
    mason = case model.result of
              Err _ -> vehicle.mason
              Ok parent -> Mason.combineParentNamespaces parent vehicle.mason
    name = vehicle.model ++ " " ++ vehicle.licensePlate
  in
    tr []
       [ td [] [ h2 [] [ text name ]
               , img [ src vehicle.image
                     , alt ("Image of " ++ name) ] [] ]
       , td [] (List.map (renderControl address) mason.controls) ]

decodeToView : Json.Decoder (Signal.Address Mason.Render.Action -> Model -> Html)
decodeToView =
  decoder `Json.andThen` \x -> Json.succeed ((flip view) x)
