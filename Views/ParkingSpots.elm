module Views.ParkingSpots (decodeToView) where

import Mason
import Mason.Render exposing (renderControl)
import Model exposing (Model)
import Json.Decode as Json exposing ((:=))
import Html exposing (table, tbody, tr, td, h2, text, Html)
import Html.Attributes exposing (class)
import Views.Geo exposing (geoView)
import Views.ParkingSpot exposing (ParkingSpot, spotView)

decoder : Json.Decoder (List ParkingSpot)
decoder =
  ("parkingSpots" := Json.list Views.ParkingSpot.decoder)

view : Signal.Address Mason.Render.Action -> List ParkingSpot -> Model -> Html
view address spots model =
  table [ class "table table-bordered" ]
          [ tbody [] <| List.map (spotView address model) spots ]

decodeToView : Json.Decoder (Signal.Address Mason.Render.Action -> Model -> Html)
decodeToView =
  decoder `Json.andThen` \x -> Json.succeed ((flip view) x)
