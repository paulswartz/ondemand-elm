module Views.ParkingSpot (ParkingSpot, spotView, decoder, decodeToView) where

import Mason
import Mason.Render exposing (renderControl)
import Model exposing (Model)
import Json.Decode as Json exposing ((:=))
import Html exposing (table, tbody, tr, td, h2, text, Html)
import Html.Attributes exposing (class)
import Views.Geo exposing (geoView)

type alias ParkingSpot =
  { name: String
  , geo : (Float, Float)
  , mason: Mason.Mason
  }


decoder : Json.Decoder ParkingSpot
decoder = Json.object3 ParkingSpot
          ( "name" := Json.string)
          ( "geo" := (Json.object2 (,)
                            ("latitude" := Json.float)
                            ("longitude" := Json.float) ) )
          Mason.decoder

view : Signal.Address Mason.Render.Action -> ParkingSpot -> Model -> Html
view address spot model =
  table [ class "table table-bordered" ]
          [ tbody [] [(spotView address model) spot]]

spotView : Signal.Address Mason.Render.Action -> Model -> ParkingSpot -> Html
spotView address model spot =
  let
    mason = case model.result of
              Err _ -> spot.mason
              Ok parent -> Mason.combineParentNamespaces parent spot.mason
  in
    tr []
         [ td [] [ h2 [] [ text spot.name ]
                 , geoView spot.geo ]
         , td [] (List.map (renderControl address) mason.controls) ]

decodeToView : Json.Decoder (Signal.Address Mason.Render.Action -> Model -> Html)
decodeToView =
  decoder `Json.andThen` \x -> Json.succeed ((flip view) x)
