module Views.Geo (geoView) where

import Html exposing (iframe, Html)
import Html.Attributes exposing (width, height, src)

googleMapsUrl geo =
  "https://www.google.com/maps/embed/v1/place?q=" ++ (toString <| fst geo) ++ "%2C" ++ (toString <| snd geo) ++ "&key=AIzaSyAJiUSy9HHLJehy12Th735pGW3QyjxMeH0"

geoView : (Float, Float) -> Html
geoView geo =
  iframe [ width 500
         , height 500
         , src (googleMapsUrl geo ) ] []
