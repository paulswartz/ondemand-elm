module Views.VehicleStatus (decodeToView) where

import Html exposing (div, pre, text, Html)
import Html.Attributes exposing (class)
import Json.Encode
import Json.Decode as Json exposing ((:=))
import Mason.Render
import Views.ProgressBar exposing (progressBar)
import Views.Geo exposing (geoView)
import Model exposing (Model)

type alias VehicleStatus =
  { gasLevel : Float
  , geo : Maybe (Float, Float)
  }



decoder : Json.Decoder VehicleStatus
decoder = Json.object2 VehicleStatus
          ( "gasLevel" := Json.float)
          ( Json.maybe ( "geo" := (Json.object2 (,)
                                         ("latitude" := Json.float)
                                         ("longitude" := Json.float) ) ) )

view : Signal.Address Mason.Render.Action -> VehicleStatus -> Model -> Html
view address status model =
  div [ class "row" ]
        [ div [ class "col-xs-12 col-md-6" ]
                [ gasLevelView status.gasLevel
                , Maybe.withDefault (div [] []) (Maybe.map geoView status.geo) ] ]

gasLevelView : Float -> Html
gasLevelView level =
  let
    percent = round (100 * level)
    percentString = (toString percent) ++ "%"
    content = ("Gas Level: " ++ percentString)
  in
    div [] [ text content
           , progressBar "" percentString content]


decodeToView : Json.Decoder (Signal.Address Mason.Render.Action -> Model -> Html)
decodeToView =
  decoder `Json.andThen` \x -> Json.succeed ((flip view) x)
