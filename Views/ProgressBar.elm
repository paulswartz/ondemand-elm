module Views.ProgressBar (progressBar) where

import Html exposing (div, span, text)
import Html.Attributes exposing (class, style)

progressBar className percentString content =
  div [ class "progress" ]
        [ div [ class ("progress-bar active " ++ className)
              , style [("width", percentString)] ]
          [ span [ class "sr-only" ] [ text content ]
          ]
        ]
