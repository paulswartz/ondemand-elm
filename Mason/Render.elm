module Mason.Render where

import Mason exposing (Control)
import Schema exposing (Property)
import Signal exposing (Address)
import Html exposing (..)
import Html.Attributes exposing (name, value, type', action, method, for, class)
import Html.Events exposing (onWithOptions, on, targetValue)
import Graphics.Element exposing (show)
import Json.Decode as Json

type Action = Activated Control
            | Updated Control

renderControl : Address Action -> Control -> Html.Html
renderControl address control =
  div [] [ renderControlTitle control
         , renderControlForm address control ]

renderControlTitle : Control -> Html.Html
renderControlTitle control =
  div [] [ h3 [] [ text (Maybe.withDefault "" control.title) ]
         , h4 [] [text (Maybe.withDefault "" control.description)]]

renderControlForm : Address Action -> Control -> Html.Html
renderControlForm address control =
  form
  [ method control.method
  , action control.href
  , onActivate address control]

  [ div [] [text control.relation]
  , div [] <| List.map (renderSchemaProperty address control) control.schema
  , button [ type' "submit"
           , class "btn btn-default" ] [text "Activate"]
  ]


renderSchemaProperty : Address Action -> Control -> Property -> Html.Html
renderSchemaProperty address control property =
  div [ class "form-group" ]
        [ renderPropertyTitle property
        , renderPropertyInput address control property]

renderPropertyTitle : Property -> Html.Html
renderPropertyTitle property =
  label [for property.name]
          [ text (Maybe.withDefault property.name property.title)
          , text (Maybe.withDefault "" <| Maybe.map (\x -> ": " ++ x) property.description)
          ]

renderPropertyInput : Address Action -> Control -> Property -> Html.Html
renderPropertyInput address control property =
  let
    newProperty s old =
      if old.name == property.name then
         Schema.updateFromString old s
      else
        old

    newValue : String -> Action
    newValue s =
      Updated { control | schema = List.map (newProperty s) control.schema }

  in
    input [ name property.name
          , type' <| propertyInputType property
          , value <| propertyValueAsString property
          , onInput address newValue
          ] []


propertyInputType : Property -> String
propertyInputType property =
  case property.value of
    Schema.JsonNumber _ -> "number"
    _ -> "text"

propertyValueAsString : Property -> String
propertyValueAsString property =
  Schema.asString property.value

onActivate : Address Action -> Control -> Attribute
onActivate address control =
  onWithOptions "submit" {
                    stopPropagation = True
                  , preventDefault = True }
  Json.value (\_ ->
                Signal.message address (Activated control))

onInput : Address a -> (String -> a) -> Attribute
onInput address textToValue =
  on "input" targetValue (\v -> Signal.message address (textToValue v))
