module Mason.Control where
import Mason exposing (Control)
import Schema exposing (asString)
import Http
import Task exposing (Task)
import Regex exposing (replace, regex)
import Dict
import Json.Encode

toTask : Control -> Task Http.Error Mason.Mason
toTask control =
  let
    url = urlFor control
    headers = headersFor control
    body = bodyFor control
  in
    Http.send Http.defaultSettings {
            verb = control.method
          , headers = headers
          , url = url
          , body = body }
    |> Http.fromJson Mason.decoder

urlFor control =
  let arguments =
        List.map (\prop -> (prop.name, asString prop.value)) control.schema
  in
    fillUriTemplate control.href arguments

headersFor control =
  let
    headers = [("Accept", "application/vnd.mason+json")]
  in
    case control.encoding of
      Mason.JSON -> headers ++ [("Content-Type", "application/json")]
      _ -> headers

bodyFor control =
  case control.encoding of
    Mason.None -> Http.empty
    Mason.JSON ->
      case control.template of
        Just template -> Http.string (Json.Encode.encode 0 template)
        Nothing -> Http.empty


fillUriTemplate : String -> List (String, String) -> String
fillUriTemplate uri arguments =
  let
    argumentDict = Dict.fromList arguments

    fillMatch : Regex.Match -> String
    fillMatch match =
      case match.submatches of
        [Just key] ->
          Dict.get key argumentDict
            |> Maybe.withDefault ""

        _ -> ""
  in
    replace Regex.All (regex "{(.+?)}") fillMatch uri
