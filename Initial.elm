module Initial (mason) where
import Mason
import Mason.Control
import Schema
import Dict
import Json.Encode

controlFor : String -> Mason.Control
controlFor url =
  Mason.Control
       "root"
       (Just <| "API @ " ++ url)
       Nothing
       "GET"
       url
       []
       Mason.None
       Nothing

urlControl : Mason.Control
urlControl =
  Mason.Control
       "root"
       (Just "Initial URL")
       (Just "The location of the API to display")
       "GET"
       "{url}"
       [Schema.Property
                "url"
                (Just "Root URL")
                Nothing
                (Schema.JsonString "")
       ]
       Mason.None
       Nothing

mason : Mason.Mason
mason =
  let
    urls = [ "http://localhost:9292/"
           , "http://ondemand-api.pswartz.savannah.localmotion.info/"
           , "http://ondemand.dogfood.getlocalmotion.com" ]
    controls = List.map controlFor urls
  in
    Mason.Mason Dict.empty (urlControl :: controls) (Json.Encode.null)
