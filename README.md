# OnDemand Elm

Lightweight interface over a Mason API

## Running

    python -m SimpleHTTPServer 8083

Then hit http://localhost:8083/

## Developing

    brew cask install elm-platform # or use http://elm-lang.org/install
    elm reactor

Then hit http://localhost:8082/Main.elm to see the interface.
